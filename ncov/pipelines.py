# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import re
import time

import pymongo

from ncov.items import PostItem, UserItem, YLItems, MaigooBrandItem


class NcovPipeline(object):
    def process_item(self, item, spider):
        return item


class WeiboPipeline(object):
    def parse_time(self, date):
        if re.match('刚刚', date):
            date = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time()))
        if re.match('\d+秒前', date):
            date = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time()))
        if re.match('\d+分钟前', date):
            minute = re.match('(\d+)', date).group(1)
            date = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time() - float(minute) * 60))
        if re.match('\d+小时前', date):
            hour = re.match('(\d+)', date).group(1)
            date = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time() - float(hour) * 60 * 60))
        if re.match('昨天.*', date):
            date = re.match('昨天(.*)', date).group(1).strip()
            date = time.strftime('%Y-%m-%d', time.localtime(time.time() - 24 * 60 * 60)) + ' ' + date
        if re.match('\d{2}-\d{2}', date):
            date = time.strftime('%Y-', time.localtime()) + date + ' 00:00'
        if re.match('\d{2}月\d{2}日 \d{2}:\d{2}', date):
            date = time.strftime('%Y年', time.localtime()) + date
            date = date.replace('年', '-').replace('月', '-').replace('日', '')
        return date

    def process_item(self, item, spider):
        if isinstance(item, PostItem) or isinstance(item, UserItem):
            if item.get('created_at'):
                item['created_at'] = item['created_at'].strip()
                item['created_at'] = self.parse_time(item.get('created_at'))
            if item.get('pics'):
                item['pics'] = [pic.get('url') for pic in item.get('pics')]
            return item
        else:
            return item


class MongoPipeline(object):
    def __init__(self, local_mongo_host, local_mongo_port, mongo_db):
        self.local_mongo_host = local_mongo_host
        self.local_mongo_port = local_mongo_port
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            local_mongo_host=crawler.settings.get('LOCAL_MONGO_HOST'),
            local_mongo_port=crawler.settings.get('LOCAL_MONGO_PORT'),
            mongo_db=crawler.settings.get('DB_NAME')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.local_mongo_host, self.local_mongo_port)
        # 数据库名
        self.db = self.client[self.mongo_db]
        # 以Item中collection命名 的集合  添加index
        self.db[PostItem.collection].create_index([('url', pymongo.ASCENDING)])
        self.db[UserItem.collection].create_index([('url', pymongo.ASCENDING)])

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if isinstance(item, PostItem) or isinstance(item, UserItem):
            self.db[item.collection].update({'url': item.get('url')},
                                            {'$set': item},
                                            True)
        if isinstance(item, YLItems):
            self.db[item.collection].update({'id': item.get('id')},
                                            {'$set': item},
                                            True)

        if isinstance(item, MaigooBrandItem):
            self.db[item.collection].update({'idBrand': item.get('idBrand')},
                                            {'$set': item},
                                            True)
        return item
