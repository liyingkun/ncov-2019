# -*- coding: utf-8 -*-

# Scrapy settings for ncov project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'ncov'

SPIDER_MODULES = ['ncov.spiders']
NEWSPIDER_MODULE = 'ncov.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'

# Obey robots.txt rules
# ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = True

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'ncov.middlewares.NcovSpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    # 'ncov.middlewares.NcovDownloaderMiddleware': 543,
    'ncov.middlewares.CookiesMiddleware': 543,

}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'ncov.pipelines.WeiboPipeline': 300,
    'ncov.pipelines.MongoPipeline': 301,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# cookies_pools
COOKIESPOOLS = [
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "30744674551",
     "SCF": "AiiYc1MNmK2xbOvX7328n-Ocw4zBIhNeXGxt0Fiw2YGAU6_IGbmuV5fMK2BJb7F1dZyIWTp5xOBSes8TdVAOq-E.",
     "SSOLoginState": "1579166707", "MLOGIN": "1", "XSRF-TOKEN": "5887d3", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "05iqQ2ruNgWe_7",
     "SUB": "_2A25zJFujDeRhGeFN6VIS-C7LyzSIHXVQ52XrrDV6PUJbkdANLWTtkW1NQE0SwZhebXRFE2-7nUoNRcLr1BZk1Xkj"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "47765954991",
     "SCF": "AgbzL-C84Lgtl1O-MZkUuUHqTUVAWMknuGujOuUgFpOTSo_57aRfYZwSK2ZMAYNwIGNeA5_j5gvtWFDu__rNwok.",
     "SSOLoginState": "1579167050", "MLOGIN": "1", "XSRF-TOKEN": "ccf49a", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0C1S0KXtV5S0Gw",
     "SUB": "_2A25zJF0aDeRhGeFM4lcU9yfJwjiIHXVQ52NSrDV6PUJbkdANLUjekW1NQIzFIk5KAUJje_VECFfS0MoSrJ9JoFbe"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "45810624710",
     "SCF": "AkRGmlrcF0hCLIUUFSRl-uu1Xmlpw24LMX8-gzMYqz7urI0nJhCznuiCFoG_c8WaH6N6XdiFSfTG6SrOD2JgZEI.",
     "SSOLoginState": "1579166714", "MLOGIN": "1", "XSRF-TOKEN": "375bdc", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "010WTINwqA-zOR",
     "SUB": "_2A25zJFuqDeRhGeFN6VIQ8S3EyzWIHXVQ52XirDV6PUJbkdANLWf7kW1NQE0SxU5jbML-siOrMmdUMur_S7mXwmi3"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "82715720381",
     "SCF": "AiuCzAASeQI0IJbAv2zZdIIMzcX6pvMWVbRaACnipMoj6QuNdcktlOYXUJtTSpf4NFyyUA700tpfwpRjllQIsqk.",
     "SSOLoginState": "1579166782", "MLOGIN": "1", "XSRF-TOKEN": "e23d86", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0ft5BNEL3GDXMz",
     "SUB": "_2A25zJFxuDeRhGeFN6VIS-C7LyzqIHXVQ52QmrDV6PUJbkdAKLVClkW1NQE0Swp3iDDi-a_i7ocnjFl3CcqydhoX-"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "74704385002",
     "SCF": "AnmGnyJPRTIkrJ201OCX_dWtYQn_LpC_dvHrcCtEWoBsDrIWxMaxuvyhBgL0Sv0w2b8Lq95-PeQzTwWJuRRacyA.",
     "SSOLoginState": "1579166798", "MLOGIN": "1", "XSRF-TOKEN": "16fbee", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0A0ZDlRHEjmrmU",
     "SUB": "_2A25zJFweDeRhGeFN6VIQ8S3EyDqIHXVQ52RWrDV6PUJbkdANLRP8kW1NQE0S3ktjAlSPJkvefsyRD1imeqHmRrwo"},
    {"WEIBOCN_FROM": "1110006030", "_T_WM": "21528164154",
     "SCF": "AlYtvUyvSDCXBMW6gKJM_0NjSUlIJgJoRSrwVYcoKXvJcHxkB-PbUYB9vtTNObYP38afkUpQ6q0PngWLCHjbqys.",
     "SSOLoginState": "1579166752", "MLOGIN": "1", "XSRF-TOKEN": "039a68", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0nl-rPRqNzFg6c",
     "SUB": "_2A25zJFxwDeRhGeFN6VIS-C7KyTmIHXVQ52Q4rDV6PUJbkdAKLW_ckW1NQE0Sz5_B3Ep-J5FTaCIU8KKTeUWS39fp"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "21328499774",
     "SCF": "AlwyqU-XG1H-ycOmejxpZeYAyDr8L2yvx8nYfTJVsDQqPkUIQm10Lf70Vn3OHJsKk0ywOLDP43sn0702y6Pa1WM.",
     "SSOLoginState": "1579166813", "MLOGIN": "1", "XSRF-TOKEN": "d74e50", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0HTxvGd7TD1jBB",
     "SUB": "_2A25zJFwMDeRhGeFN6VIQ8S3EzjqIHXVQ52RErDV6PUJbkdAKLUyskW1NQE0S23lS-05idLxGMRVqcxTp4gU0KeCH"},
    {"SSOLoginState": "1579166964",
     "SCF": "ArGgiBHGlYh_TR3iXq0iB1b1AL0znl30g_DX1x5SJUfyBhbeDmV_unzBPUX-NK5qtid49tYEB5etUFOilSWpU3o.",
     "SUB": "_2A25zJFykDeRhGeFM4lcU-CnFwjqIHXVQ52TsrDV6PUJbkdANLVbwkW1NQIzD2zIzCgTLUFW20mOrXhsM-A3cneYs",
     "XSRF-TOKEN": "d74e50", "M_WEIBOCN_PARAMS": "uicode%3D20000174", "SUHB": "0sqR-m6OyKbPG6", "MLOGIN": "1"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "66535481497",
     "SCF": "Ajh_PtB9CCE-no1nFLzTXccxxWaAxT3Amxki318J6KjjalI1pf_a0b8AlPo0kgCG4msPxqvXKCNqb8vzLTBVYRs.",
     "SSOLoginState": "1579166986", "MLOGIN": "1", "XSRF-TOKEN": "0c25f3", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0y0JHkr95jmVgy",
     "SUB": "_2A25zJF1aDeRhGeFM4lcQ8ibLzTmIHXVQ52MSrDV6PUJbkdAKLWnhkW1NQIzD31bqNn6dWmYa0FEJZq0A-DzKZczt"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "93038165465",
     "SCF": "Amg78w8C92CSRDaoI08euuPvlDhbxcKqgOpSP1IoZp8PYqAlcGhKp6Quv_-gOhlWh3H9UbypNt-7C4nWmseo284.",
     "SSOLoginState": "1579166745", "MLOGIN": "1", "XSRF-TOKEN": "ea55b3", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0C1S3LiTZ5S0Gw",
     "SUB": "_2A25zJFxJDeRhGeFN6VIS-C7Lyj6IHXVQ52QBrDV6PUJbkdAKLUvNkW1NQE0SwYRAiLoly5SVA9ctPeiLYWKnMOEP"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "93090839557",
     "SCF": "ApYl650WNAxHUnGu0aO2oq2QVdcU754-ktTeLIzREcMxcq_nVzfFuIfk5p-5vjEbsoNLXMJeRp2B42fOwTgvN9o.",
     "SSOLoginState": "1579167024", "MLOGIN": "1", "XSRF-TOKEN": "0b61b5", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0U1ZDefgNZOjsN",
     "SUB": "_2A25zJF1gDeRhGeFM4lcQ8i7KyzSIHXVQ52MorDV6PUJbkdAKLWv-kW1NQIzFLibA2m2OrU0EjDS7ZKNOFXXUJLDC"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "92933495115",
     "SCF": "Amjj-unlhiy7u9fCY5Ie96rNovgjWsNINzkykNJPkmQZxWz5d1lsa1Fy6Naq9UtG5xqmWhdS0LyJuarpCgYJ2gg.",
     "SSOLoginState": "1579166692", "MLOGIN": "1", "XSRF-TOKEN": "39d611", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0auQOvF4N0iY4E",
     "SUB": "_2A25zJFu0DeRhGeFN6VIQ8S3EyTqIHXVQ52X8rDV6PUJbkdANLXP4kW1NQE0SwI5vqgIx9S00ZWFwtzz7d2lvgIYM"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "52314623591",
     "SCF": "AlE6OMRUd4oU58wK5K64BpHYAWgfETJnAcY3uf4uYDArprKU-_eYqfXtZvluK0DwgQ7tG8ubSHlzQe-e9NZ9aL4.",
     "SSOLoginState": "1579167016", "MLOGIN": "1", "XSRF-TOKEN": "4da3e0", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0laVEuf_dbkueo",
     "SUB": "_2A25zJF14DeRhGeFM4lcQ8i7KzDyIHXVQ52MwrDV6PUJbkdANLULNkW1NQIzFImoMtcVM_bqvVIHzFRmoGWZKtrnB"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "24219351106",
     "SCF": "ApyqmjtCq0nGY7ZLOf2sdltq_qo05ciHuhXaG7MEMtD-zgJR7378fCUD-YoUJAHmGaWQF3BzE6xF12IMqp5VnII.",
     "SSOLoginState": "1579166767", "MLOGIN": "1", "XSRF-TOKEN": "405c06", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0z1Q-BDo-QCXXR",
     "SUB": "_2A25zJFx_DeRhGeFN6VIQ8S3EyTWIHXVQ52Q3rDV6PUJbkdAKLWyhkW1NQE0SwJ0xAgnj7e98PV8U_d0gbo7qwjiZ"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "34616130280",
     "SCF": "Ahsm_EQ72m-rcx5ffvJlk_QvSeuXWB2JHxIHTdhwPIR0M2AwqWrTHyR7Goex7hCYx_9_AmxyDwL3L6hbhL4T8AE.",
     "SSOLoginState": "1579166775", "MLOGIN": "1", "XSRF-TOKEN": "a520a2", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0U1ZAfqeRZOjsN",
     "SUB": "_2A25zJFxnDeRhGeFN6VIS-C7KzTiIHXVQ52QvrDV6PUJbkdAKLWHwkW1NQE0SyYCS74aTyp9DOLYoDNkd5qjqgEIP"},
    {"WEIBOCN_FROM": "1110006030", "_T_WM": "38283905551",
     "SCF": "Ao1ls0fcdGAU2-xOJ8K2aWN3njkxS7iOfJphG1u2xIsu4xgjjfThKm17E3fe2CLtkWgPS-1f5qaFcqE7L7mU4h8.",
     "SSOLoginState": "1579167001", "MLOGIN": "1", "XSRF-TOKEN": "c61c9f", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0FQ73Be248q5XT",
     "SUB": "_2A25zJF1JDeRhGeFM4lcQ8i7Eyz2IHXVQ52MBrDV6PUJbkdAKLWj4kW1NQIzFzWmGedj938-7WdeIp-qVEeARmNwF"},
    {"WEIBOCN_FROM": "1110106030", "MLOGIN": "1", "XSRF-TOKEN": "3c8d93", "SUHB": "0gHynGFybXOuK9",
     "M_WEIBOCN_PARAMS": "uicode%3D20000174", "_T_WM": "19392749917", "SSOLoginState": "1579166972",
     "SCF": "AondnHy4k_1TkQoXXHvHHh9bZLdYaSuFFa0Ga2z0rBgPs-YR_89qaP6AkumvYP_d2xSCzr6_5i5hw7MGHs6qpc4.",
     "SUB": "_2A25zJFyrDeRhGeFM4lcQ8ibLwzWIHXVQ52TjrDV6PUJbkdANLVrQkW1NQIzD3I7UaY6b6OoF9ujZet3eRVv9Oor4"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "60478500687",
     "SCF": "AlfgHX0uOAK9Qbv8nvffsbUF7yTKEEHT-kpkO8ZcxjRT8He34tCi7SofytavQIYwDzVX38Ge8gMydmmvWKtGncQ.",
     "SSOLoginState": "1579166722", "MLOGIN": "1", "XSRF-TOKEN": "c0a854", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0qjkyfxArhT3cq",
     "SUB": "_2A25zJFxSDeRhGeFN6VIQ8S3Ezj2IHXVQ52QarDV6PUJbkdANLVLskW1NQE0S3JgDCUSiGfJflW99CANUQGwedjAg"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "83099233983",
     "SCF": "AhU322gmd5aVooabyKV_Jk9JUF8n0n5MML_ZglzDgXGagG4BXrYP3nPb_O7s3J54pA2QQoS2wAuqkY6jB5iChuc.",
     "SSOLoginState": "1579166790", "MLOGIN": "1", "XSRF-TOKEN": "adfd88", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "010WTINwKA-zOR",
     "SUB": "_2A25zJFwWDeRhGeFN6VIQ8S3LzTWIHXVQ52RerDV6PUJbkdANLXXAkW1NQE0SzUhmDhRJINCG2u0k1sdtyW6dPgID"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "18006064007",
     "SCF": "AiogJAU7pfxciyvhxZUkLzuSK6qMevuJZHaE7R8EQzEXB6i2CkM6a86shq1QC9kYYrpTgEmYjAZqumyqhxqszMQ.",
     "SSOLoginState": "1579166994", "MLOGIN": "1", "XSRF-TOKEN": "31f3a6", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "079zkzXAJmzorT",
     "SUB": "_2A25zJF1CDeRhGeFM4lcU-CnEwz-IHXVQ52MKrDV6PUJbkdAKLUynkW1NQIzDzlBToURWGfRTqDtOqpabihu7SE0N"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "18006064007",
     "SCF": "AiogJAU7pfxciyvhxZUkLzuSK6qMevuJZHaE7R8EQzEXB6i2CkM6a86shq1QC9kYYrpTgEmYjAZqumyqhxqszMQ.",
     "SSOLoginState": "1579166994", "MLOGIN": "1", "XSRF-TOKEN": "31f3a6", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "079zkzXAJmzorT",
     "SUB": "_2A25zJF1CDeRhGeFM4lcU-CnEwz-IHXVQ52MKrDV6PUJbkdAKLUynkW1NQIzDzlBToURWGfRTqDtOqpabihu7SE0N"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "39010181248",
     "SCF": "Apu3Tp0fTcb56OlZGWvCCjxjKZfEUi_5j1JBUk_pU3jRYhld45hgeL-C4uGHYbGglV1g_0ZiT48zQ1VrFamqRcA.",
     "SSOLoginState": "1579166729", "MLOGIN": "1", "XSRF-TOKEN": "3b76de", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0xnSDSRJ4wxnMa",
     "SUB": "_2A25zJFxZDeRhGeFN6VIS-C7KyT2IHXVQ52QRrDV6PUJbkdAKLU3skW1NQE0SMCLvpYZ9PlZnSJlW0ZCHTUdZEYnU"},
    {"MLOGIN": "1", "WEIBOCN_FROM": "1110006030",
     "SCF": "AnzOHce7XbJBUaBVXS-sjVjVIKp0TUkpzSPceYPEj1LvdoNUZ8Cew0m_y8simizAAq7BZmo9l27SYuwbsbIem0E.",
     "_T_WM": "58687071560", "SSOLoginState": "1579166684", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "XSRF-TOKEN": "d77ad1", "SUHB": "0FQ70ArGY8q5XT",
     "SUB": "_2A25zJFuMDeRhGeFN6VIS-C7KwzqIHXVQ52XErDV6PUJbkdAKLU3bkW1NQE0SxUBsrFRJUV6_Ow-HePpkbvgDj2V3"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "36402723338",
     "SCF": "AuvbR_iukpnogPZzIP6KwpHoE1_MsOuB-T5nmX5C31XWZFq2zi0Nn0dLqi2nLrAvsQkFnu4528vwYmoZlAIINUA.",
     "SSOLoginState": "1579166737", "MLOGIN": "1", "XSRF-TOKEN": "df9032", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0rrrc3kMwD09f5",
     "SUB": "_2A25zJFxBDeRhGeFN6VIS-C7KzjSIHXVQ52QJrDV6PUJbkdAKLWbukW1NQE0Syph_u4SyO35A0QHttEecXcTtiLtS"},
    {"WEIBOCN_FROM": "1110106030", "_T_WM": "36402723338",
     "SCF": "AuvbR_iukpnogPZzIP6KwpHoE1_MsOuB-T5nmX5C31XWZFq2zi0Nn0dLqi2nLrAvsQkFnu4528vwYmoZlAIINUA.",
     "SSOLoginState": "1579166737", "MLOGIN": "1", "XSRF-TOKEN": "df9032", "M_WEIBOCN_PARAMS": "uicode%3D20000174",
     "SUHB": "0rrrc3kMwD09f5",
     "SUB": "_2A25zJFxBDeRhGeFN6VIS-C7KzjSIHXVQ52QJrDV6PUJbkdAKLWbukW1NQE0Syph_u4SyO35A0QHttEecXcTtiLtS"},
    {'_s_tentry': 'news.my399.com', 'Apache': '2187920640947.4094.1558967089391',
     'SINAGLOBAL': '2187920640947.4094.1558967089391', 'ULV': '1558967089397:1:1:1:2187920640947.4094.1558967089391:',
     'login_sid_t': '542022a6fbd9e10665f2e4f619691bdc', 'cross_origin_proto': 'SSL',
     '_ga': 'GA1.2.707123865.1572854361', '__gads': 'ID', 'SSOLoginState': '1577774259',
     'UOR': 'news.my399.com,widget.weibo.com,www.google.com', 'wvr': '6',
     'SUBP': '0033WrSXqPxfM725Ws9jqgMF55529P9D9WhQ6_Ew7CxR_r.eb3ZzXFi65JpX5KMhUgL.Foqf1hzc1hMRehz2dJLoIEBLxK-LBo5L12qLxKML1K5LBoBLxK-L12qLBo5LxKML1-eL1h.t',
     'ALF': '1612403973',
     'SCF': 'Am0bbrmldos7g4R7KXDyFO2n1TbtfOUKnCvE59yyoZJTkfZftDSajmZcHhsGwPhaDdc3At9ORSE3N_hIoW0YXi8.',
     'SUB': '_2A25zPlHYDeRhGeBL41AX-CnEyz6IHXVQSsQQrDV8PUNbmtANLVDQkW9NRsQP-Qv1YFKwLXrsmWeX4GcvaR3IBr46',
     'SUHB': '0qjkoW2S-_d6cu', 'WBStorage': '42212210b087ca50|undefined',
     'webim_unReadCount': '%7B%22time%22%3A1580905361159%2C%22dm_pub_total%22%3A0%2C%22chat_group_client%22%3A0%2C%22allcountNum%22%3A41%2C%22msgbox%22%3A0%7D'}
]

# mongo
LOCAL_MONGO_HOST = 'mongodb://root:dnqW9MriFu5P2x@120.76.101.229:27017/?authSource=admin'
LOCAL_MONGO_PORT = None
DB_NAME = 'ncov'

# 遇到这些错误码就重新发送请求
RETRY_HTTP_CODES = [302, 401, 403, 408, 414, 500, 502, 503, 504]
RETRY_TIMES = 5

# ----------- selenium参数配置 -------------
SELENIUM_TIMEOUT = 25  # selenium浏览器的超时时间，单位秒
LOAD_IMAGE = False  # 是否下载图片
WINDOW_HEIGHT = 900  # 浏览器窗口大小
WINDOW_WIDTH = 900
