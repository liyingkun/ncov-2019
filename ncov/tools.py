# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/4 1:24 下午
# @File     :tools.py
# @Software :PyCharm
import uuid


def getKolID(platform, platformUID):
    """
    计算kolID

    计算方式为：platform + _ + platformUID uuid3 dns
    :param platform: 社交平台名称
    :param platformUID: 社交平台对用户的唯一标识
    :return:
    """
    temp = platform + '_' + str(platformUID)
    kolID = str(uuid.uuid3(uuid.NAMESPACE_DNS, temp))
    return kolID


ckstr = "_s_tentry=news.my399.com; Apache=2187920640947.4094.1558967089391; SINAGLOBAL=2187920640947.4094.1558967089391; ULV=1558967089397:1:1:1:2187920640947.4094.1558967089391:; login_sid_t=542022a6fbd9e10665f2e4f619691bdc; cross_origin_proto=SSL; _ga=GA1.2.707123865.1572854361; __gads=ID=19b53d7b07373869:T=1572854363:S=ALNI_Mb8xLQF09YHbeMCbvxCD3G3p86dxw; SSOLoginState=1577774259; UOR=news.my399.com,widget.weibo.com,www.google.com; wvr=6; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WhQ6_Ew7CxR_r.eb3ZzXFi65JpX5KMhUgL.Foqf1hzc1hMRehz2dJLoIEBLxK-LBo5L12qLxKML1K5LBoBLxK-L12qLBo5LxKML1-eL1h.t; ALF=1612403973; SCF=Am0bbrmldos7g4R7KXDyFO2n1TbtfOUKnCvE59yyoZJTkfZftDSajmZcHhsGwPhaDdc3At9ORSE3N_hIoW0YXi8.; SUB=_2A25zPlHYDeRhGeBL41AX-CnEyz6IHXVQSsQQrDV8PUNbmtANLVDQkW9NRsQP-Qv1YFKwLXrsmWeX4GcvaR3IBr46; SUHB=0qjkoW2S-_d6cu; WBStorage=42212210b087ca50|undefined; webim_unReadCount=%7B%22time%22%3A1580905361159%2C%22dm_pub_total%22%3A0%2C%22chat_group_client%22%3A0%2C%22allcountNum%22%3A41%2C%22msgbox%22%3A0%7D"
ckdict = {}
for item in ckstr.split(';'):
    _ = item.strip().split('=')
    ckdict[_[0]] = _[1]
