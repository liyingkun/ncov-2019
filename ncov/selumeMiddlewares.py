# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/27 9:47 下午
# @File     :selumeMiddlewares.py
# @Software :PyCharm

# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from scrapy.http import HtmlResponse
from logging import getLogger
import time


class SeleniumMiddleware():
    # Middleware中会传递进来一个spider，这就是我们的spider对象，从中可以获取__init__时的chrome相关元素
    def process_request(self, request, spider):
        '''
        用chrome抓取页面
        :param request: Request请求对象
        :param spider: Spider对象
        :return: HtmlResponse响应
        '''
        print(f"chrome is getting page")
        # 依靠meta中的标记，来决定是否需要使用selenium来爬取
        usedSelenium = request.meta.get('usedSelenium', False)
        if usedSelenium:
            try:
                spider.browser.get(request.url)

                # morebtnText = spider.wait.until(
                #     EC.presence_of_element_located((By.XPATH, '//*[@class="morebtn c333 ajaxload"]/span'))
                # ).text
                # if morebtnText != '加载更多':
                #     print('没有更多内容了')
                #     raise ValueError('没有更多内容了')

                n = 0
                flag = True
                while flag and n < 10:
                    # 向下翻页
                    spider.browser.find_element_by_tag_name('body').send_keys(Keys.END)
                    # 点击加载更多
                    morebtn = spider.wait.until(
                        EC.presence_of_element_located((By.XPATH, '//*[@class="morebtn c333 ajaxload"]'))
                    )
                    time.sleep(3)
                    # 查看是否还有更多
                    morebtn.click()  # 模拟点击,可以模拟点击加载更多
                    # 查看是否还有内容
                    searchRes = spider.wait.until(
                        EC.presence_of_element_located((By.XPATH, '//div[@class="brandbang"]'))
                    )
                    morebtnText = spider.wait.until(
                        EC.presence_of_element_located((By.XPATH, '//*[@class="morebtn c333 ajaxload"]/span'))
                    ).text
                    n += 1
                    print(n)
                    if morebtnText != '加载更多':
                        flag = False

                # 搜索框是否出现
                # input = spider.wait.until(
                #     EC.presence_of_element_located((By.XPATH, "//div[@class='nav-search-field ']/input"))
                # )
                # time.sleep(2)
                # input.clear()
                # input.send_keys("iphone 7s")
                # # 敲enter键, 进行搜索
                # input.send_keys(Keys.RETURN)
                # # 查看搜索结果是否出现
                # searchRes = spider.wait.until(
                #     EC.presence_of_element_located((By.XPATH, "//div[@id='resultsCol']"))
                # )
            except Exception as e:
                print(f"chrome getting page error, Exception = {e}")
                return HtmlResponse(url=request.url, status=500, request=request)
            else:
                time.sleep(3)
                # 页面爬取成功，构造一个成功的Response对象(HtmlResponse是它的子类)
                return HtmlResponse(url=request.url,
                                    body=spider.browser.page_source,
                                    request=request,
                                    # 最好根据网页的具体编码而定
                                    encoding='utf-8',
                                    status=200)
