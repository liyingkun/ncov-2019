# -*- coding: utf-8 -*-
import json

import scrapy
from scrapy import Selector
import urllib.parse
import re

from scrapy.crawler import CrawlerProcess

from ncov.items import PostItem, UserItem
from ncov.spiders.keywords import keywords, keywords_dict
from ncov.tools import getKolID

base_url = 'https://s.weibo.com/weibo?q={query}&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-23-0:2020-02-25-0'
base_hot_url = 'https://s.weibo.com/weibo?q={query}&xsort=hot&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-13-0:2020-02-25-0'
base_mt_url = 'https://s.weibo.com/weibo?q={query}&category=4&suball=1&sudaref=s.weibo.com&Refer=SWeibo_box&timescope=custom:2020-01-13-0:2020-02-25-0'
ncovkeywords = "疫情|武汉|捐|赠|肺炎|口罩|钟南山|李兰娟|病毒|支援|加油|湖北"  # 新增
start_urls_ = [base_url.format(query=urllib.parse.quote(word)) for word in keywords]
start_urls_dict = {}
for k, v in keywords_dict.items():
    for word in v:
        # 不加疫情关键字,全部贴
        url = base_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 不加不加疫情关键字,热门贴
        url = base_hot_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 不加疫情关键字,媒体报告贴
        url = base_mt_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 加疫情关键字,全部贴
        word_ = word + '|{}'.format(ncovkeywords)
        url = base_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k
        # 加疫情关键字,热门贴
        # word = word + '|{}'.format(ncovkeywords)
        url = base_hot_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k
        # 加疫情关键字,媒体报告贴
        # word = word + '|{}'.format(ncovkeywords)
        url = base_mt_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k


class SweiboSpider(scrapy.Spider):
    name = 'sweibo'
    allowed_domains = ['weibo.com', 'm.weibo.cn']
    start_urls = start_urls_dict
    user_url = 'https://m.weibo.cn/api/container/getIndex?uid={uid}&type=uid&value={uid}&containerid=100505{uid}'
    custom_settings = {
        "DB_NAME": "ncov0206"
    }

    def start_requests(self):
        for url, industry in self.start_urls.items():
            yield scrapy.Request(url, dont_filter=True, meta={"industry": industry, 'dont_redirect': True,
                                                              'handle_httpstatus_list': [302]})

    def parse(self, response):
        industry = response.meta.get("industry")
        postitem = PostItem()
        html = Selector(response)
        # x = ss.xpath('//div[@class="card"]').extract()
        # x = ss.xpath('.//div[@class="card"]')
        divs = html.xpath('.//div[@class="card"]')
        ls = []
        for div in divs:
            avator = div.xpath('./div[@class="card-feed"]/div/a/img/@src').extract_first()
            name = div.xpath('.//div["content"]//a[@class="name"]')
            weiboId = name.xpath('@href').extract_first().split('?')[0].split('/')[3]
            userName = name.xpath('.//text()').extract_first()
            full_content = div.xpath('.//div["content"]//p[@node-type="feed_list_content_full"]')
            sub_content = div.xpath('.//div["content"]//p[@node-type="feed_list_content"]')
            created_at = div.xpath('./div["card-feed"]/div["content"]/p[@class="from"]/a//text()').extract_first()
            repost = div.xpath('./div[@class="card-act"]/ul/li[2]/a/text()').extract_first().strip().split(' ')
            if len(repost) == 1:
                repost = 0
            else:
                repost = repost[1]
            comment = div.xpath('./div[@class="card-act"]/ul/li[3]/a/text()').extract_first().strip().split(' ')
            if len(comment) == 1:
                comment = 0
            else:
                comment = comment[1]
            attitude = div.xpath('./div[@class="card-act"]/ul/li[4]/a/em/text()').extract_first()
            if not attitude:
                attitude = 0
            if full_content:
                content = full_content.xpath('string(.)').extract_first()
            else:
                content = sub_content.xpath('string(.)').extract_first()

            image = div.xpath('.//ul/li/img/@src').extract_first('')

            # print(userName, image)
            if not image:
                ww = div.xpath('.//div[@class="thumbnail"]/a/@action-data').extract_first()
                if ww:
                    video_imageL = [i for i in ww.split('&') if 'cover_img=' in i]
                    if video_imageL:
                        video_image = video_imageL[0].split('=')[1]
                        image = urllib.parse.unquote(video_image, encoding='utf-8', errors='replace')
            if re.match(r'^//', image):
                imageUrl = 'https:' + image
            else:
                imageUrl = image
            # print(imageUrl)
            postUrl = 'https:' + div.xpath('.//p[@class="from"]/a/@href').extract_first('')
            _ = {"avator": avator, "userName": userName, "weiboId": weiboId,
                 "content": content, "imageUrl": imageUrl, "postUrl": postUrl, 'repost': repost, 'comment': comment,
                 'attitudes': attitude, 'created_at': created_at}
            ls.append(_)
        print(ls)

        for section in ls:
            url = section.get('postUrl')
            thumbnail = section.get('imageUrl')
            title = section.get('content').strip()
            content = section.get('content').strip()
            # embedded = embedded_url.format(videoid='')
            channelId = section.get('weiboId')
            repost = section.get('repost')
            comment = section.get('comment')
            attitudes = section.get('attitudes')
            # append result
            _temp = {'search_url': response.url,
                     'url': url,
                     'title': title,
                     'content': content,
                     # 'template': 'videos.html',
                     # 'embedded': embedded,
                     'thumbnail': thumbnail,
                     'channelId': channelId,
                     'repost': repost,
                     'comment': comment,
                     'attitudes': attitudes, 'created_at': section.get('created_at')}
            for k, v in _temp.items():
                postitem[k] = v
            postitem['industry'] = industry
            yield postitem
            # 新增weibo user接口的请求
            # setids = list(set([r.get('channelId') for r in results]))

            # for weiboid in setids:
            yield scrapy.Request(url=self.user_url.format(uid=channelId), callback=self.paser_user,
                                 meta={"postitem": dict(postitem)})
        page = response.meta.get("page", 1)
        if ls:
            if re.match('&page=', response.url):
                url_ = response.url[:-7] + '&page={page}'
            else:
                url_ = response.url + '&page={page}'
            page += 1
            if page <= 10:
                # 此处有一个page堆叠
                yield scrapy.Request(url=url_.format(page=page), callback=self.parse, dont_filter=True,
                                     meta={"page": page, 'dont_redirect': True,
                                           'handle_httpstatus_list': [302]})
                self.logger.info("===开始爬取第{}页时内容===".format(page))
            else:
                self.logger.info("###10页内容爬取完成####".format(page))
        else:
            self.logger.info("当爬取第{}页时没有爬取到内容".format(page))

    def paser_user(self, response):
        postitem = response.meta.get('postitem', {})
        user_results = [json.loads(response.text).get('data', {}).get('userInfo', {})]
        if postitem and user_results:
            user_dict = {}
            if user_results:
                for res in user_results:
                    #         # 联系方式添加
                    #         description = res.get('description', '')
                    #         contacts = mex.get_contacts(description)
                    res['contacts'] = []
                    user_dict[str(res.get('id'))] = res

            result = postitem
            result['userItems'] = user_dict.get(result.get('channelId'), {})
            uitem = UserItem()
            avator = 'https://res.cloudinary.com/twodm/image/fetch/' + result.get('userItems', {}).get('avatar_hd',
                                                                                                       '')
            displayName = result.get('userItems', {}).get('screen_name')
            fans = result.get('userItems', {}).get('followers_count')
            result['influencerInfo'] = {"avator": avator, "displayName": displayName, "fans": fans,
                                        "platform": "weibo", "kolID": getKolID("weibo", result.get("channelId"))}
            post_num = result.get('userItems', {}).get('statuses_count')
            followers_count = fans
            follow_count = result.get('userItems', {}).get('follow_count')
            result['statisticsInfo'] = {
                "weibo": {"post_num": post_num, "followers_count": followers_count,
                          "follow_count": follow_count}}
            result['postInfo'] = {"title": result.get('title'), "postUrl": result.get("url"),
                                  "postImage": 'https://res.cloudinary.com/twodm/image/fetch/' + result.get(
                                      "thumbnail",
                                      ''), "repost": result.get('repost'), 'comment': result.get('comment'),
                                  'attitudes': result.get('attitudes')}
        print("===" * 10, result.keys())
        for k, v in result.items():
            uitem[k] = v

        yield uitem


if __name__ == "__main__":
    from scrapy.settings import Settings

    settings = Settings()
    settings_module_path = r'ncov.settings'
    settings.setmodule(settings_module_path, priority='project')
    process = CrawlerProcess(settings)
    process.crawl('sweibo')
    process.start()
