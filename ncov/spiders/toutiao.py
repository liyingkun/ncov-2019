# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/7 4:50 下午
# @File     :toutiao.py
# @Software :PyCharm
""" 爬取头条 """
import json

import scrapy
from scrapy import Selector
import urllib.parse
import re

from scrapy.crawler import CrawlerProcess

from ncov.items import PostItem, UserItem
from ncov.spiders.keywords import keywords, keywords_dict
from ncov.tools import getKolID

base_url = 'https://s.weibo.com/weibo?q={query}&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-01-0:2020-02-06-0'
base_hot_url = 'https://s.weibo.com/weibo?q={query}&xsort=hot&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-01-0:2020-02-06-0'
base_mt_url = 'https://s.weibo.com/weibo?q={query}&category=4&suball=1&sudaref=s.weibo.com&Refer=SWeibo_box&timescope=custom:2020-01-01-0:2020-02-06-0'
ncovkeywords = "疫情|武汉|捐|赠|肺炎|口罩|钟南山|李兰娟|病毒|支援|加油|湖北"  # 新增
start_urls_ = [base_url.format(query=urllib.parse.quote(word)) for word in keywords]
start_urls_dict = {}
for k, v in keywords_dict.items():
    for word in v:
        # 不加疫情关键字,全部贴
        url = base_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 不加不加疫情关键字,热门贴
        url = base_hot_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 不加疫情关键字,媒体报告贴
        url = base_mt_url.format(query=urllib.parse.quote(word))
        start_urls_dict[url] = k
        # 加疫情关键字,全部贴
        word_ = word + '|{}'.format(ncovkeywords)
        url = base_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k
        # 加疫情关键字,热门贴
        # word = word + '|{}'.format(ncovkeywords)
        url = base_hot_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k
        # 加疫情关键字,媒体报告贴
        # word = word + '|{}'.format(ncovkeywords)
        url = base_mt_url.format(query=urllib.parse.quote(word_))
        start_urls_dict[url] = k


class SweiboSpider(scrapy.Spider):
    name = 'toutiao'
    allowed_domains = ['weibo.com', 'm.weibo.cn']
    start_urls = start_urls_dict
    user_url = 'https://m.weibo.cn/api/container/getIndex?uid={uid}&type=uid&value={uid}&containerid=100505{uid}'
    custom_settings = {
        "DB_NAME": "ncov0206"
    }
