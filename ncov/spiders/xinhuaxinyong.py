# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/7 5:18 下午
# @File     :xinhuaxinyong.py
# @Software :PyCharm

""" 爬取头条导流的新华信用医疗公司信息 数据来源：http://m.credit100.com/#/vendorquery"""
import json

import scrapy
from scrapy import Selector
import urllib.parse
import re

from scrapy.crawler import CrawlerProcess

from ncov.items import YLItems


class XinhuaxinyongSpider(scrapy.Spider):
    name = 'xinhuaxinyong'
    allowed_domains = ['m.credit100.com']
    start_urls = [
        'http://m.credit100.com/xhsCreditApi/v1/esmongodb/xhsRest/queryYlqxCompanyByPage?pageNo=1&pageSize=2000&province=&productType=&origin=']
    custom_settings = {
        "DB_NAME": "yiliao",
        "COOKIES_ENABLED": False,
        "DEFAULT_REQUEST_HEADERS": {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cookie': "acw_tc=7b39758315810659976892472ee111f6e259e6d5a7584a4bc79be45c3bd0c2; insert_cookie=98184645; auth-token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NzE3NzhmNC04ZmI4LTQ1ODItYjI4NC0xMTcwYmQ2ZDEwYzYiLCJ1c2VySWQiOiI3NzE3NzhmNC04ZmI4LTQ1ODItYjI4NC0xMTcwYmQ2ZDEwYzYiLCJ1c2VyTmFtZSI6IlRvdXJpc3QiLCJjb21wYW55SWQiOiIiLCJjdXN0b21lclJvbGVJZCI6IiIsInVzZXJUeXBlIjoidG91cmlzdCIsImlhdCI6MTU4MTA2NjAwMH0.ApiejBy1Ozwhvd7uCuaFGXrvLQt5i9ll02GS4deaA98; auth-tourist=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NzE3NzhmNC04ZmI4LTQ1ODItYjI4NC0xMTcwYmQ2ZDEwYzYiLCJ1c2VySWQiOiI3NzE3NzhmNC04ZmI4LTQ1ODItYjI4NC0xMTcwYmQ2ZDEwYzYiLCJ1c2VyTmFtZSI6IlRvdXJpc3QiLCJjb21wYW55SWQiOiIiLCJjdXN0b21lclJvbGVJZCI6IiIsInVzZXJUeXBlIjoidG91cmlzdCIsImlhdCI6MTU4MTA2NjAwMH0.ApiejBy1Ozwhvd7uCuaFGXrvLQt5i9ll02GS4deaA98; _uab_collina=158106600092090482701595; u_asec=099%23KAFEmGEKE64Eh7TLEEEEEpEQz0yFD60JSX9oG6fJDcyqW6t1ZXs7n6PXE7EFD67EE3QTEEx6zIywjYFETrZtt3illuQTEEy%2F%2F3yDZcToE7EIsyaDgBiwSu9bE7EUsyaSSlllsyaP%2F390lllrccZdd8Rllu88syaCM%2FllWylP%2F3K%2BlllridMTEwl5dE1RaquYScsTNsMt%2Beywb64W8RWuuz8sDzXj95uAnsw9cyY31Wy9%2FqYWcFTtuVyW8i8CCGCY4Xi5Hsh6w9%2FUE7TxE5YSD5A4D4aqma2GkLJqmdeI0yE%2BRHvkiqAkWhj2iqABC%2BvUI%2BA0DYDB6OT2qH9pkfMWVNGbkf0qaHyU0HU6mdz05coGBEroiDN7VafoaGZ6w67TEEiP%2F3BG"
        },
        "DOWNLOADER_MIDDLEWARES": {
            # 'ncov.middlewares.NcovDownloaderMiddleware': 543,
            # 'ncov.middlewares.CookiesMiddleware': 543,

        }
    }

    def parse(self, response):
        res = json.loads(response.text)
        data = res.get("returnData")
        yl = YLItems()
        for item in data:
            for k, v in item.items():
                yl[k] = v
            yield yl


if __name__ == "__main__":
    from scrapy.settings import Settings

    settings = Settings()
    settings_module_path = r'ncov.settings'
    settings.setmodule(settings_module_path, priority='project')
    process = CrawlerProcess(settings)
    process.crawl('xinhuaxinyong')
    process.start()
