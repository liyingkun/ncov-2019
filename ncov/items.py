# -*- coding= utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in=
# https=//docs.scrapy.org/en/latest/topics/items.html
import uuid

import scrapy


class NcovItem(scrapy.Item):
    # define the fields for your item here like=
    # name = scrapy.Field()
    pass


class PostItem(scrapy.Item):
    collection = 'posts'
    industry = scrapy.Field()
    search_url = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    thumbnail = scrapy.Field()
    channelId = scrapy.Field()
    repost = scrapy.Field()
    comment = scrapy.Field()
    attitudes = scrapy.Field()
    created_at = scrapy.Field()


class UserItem(scrapy.Item):
    collection = 'user'
    search_url = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    thumbnail = scrapy.Field()
    channelId = scrapy.Field()
    repost = scrapy.Field()
    comment = scrapy.Field()
    attitudes = scrapy.Field()
    userItems = scrapy.Field()
    influencerInfo = scrapy.Field()
    statisticsInfo = scrapy.Field()
    postInfo = scrapy.Field()
    created_at = scrapy.Field()
    industry = scrapy.Field()


class YLItems(scrapy.Item):
    collection = 'yl'
    id = scrapy.Field()
    companyId = scrapy.Field()
    companyName = scrapy.Field()
    creditCode = scrapy.Field()
    productCat = scrapy.Field()
    location = scrapy.Field()
    province = scrapy.Field()
    productName = scrapy.Field()
    registrationNo = scrapy.Field()
    address = scrapy.Field()
    phone = scrapy.Field()
    website = scrapy.Field()
    approvedDate = scrapy.Field()
    validDate = scrapy.Field()
    proAddress = scrapy.Field()
    ylqxClass = scrapy.Field()
    belongOrg = scrapy.Field()


class MaigooBrandItem(scrapy.Item):
    collection = 'MaigooBrandItem'
    industry = scrapy.Field()
    idBrand = scrapy.Field()  # 进行UUID 货为brandLink
    logoPic = scrapy.Field()
    brandLink = scrapy.Field()
    brandName = scrapy.Field()
    companyName = scrapy.Field()
    descText = scrapy.Field()
    wdDict = scrapy.Field()
    # creatTime = scrapy.Field() # 创建时间 让其藏在objectId 里面
    updateTime = scrapy.Field()

    def getIdBrand(self, brandLink):
        return str(uuid.uuid3(uuid.NAMESPACE_DNS, brandLink))
