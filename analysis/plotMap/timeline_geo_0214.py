# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/14 6:03 下午
# @File     :timeline_geo_0214.py
# @Software :PyCharm

from pyecharts.charts import Timeline, Map, Geo
from pyecharts.faker import Faker
from pyecharts.globals import ChartType, SymbolType
from pyecharts import options as opts

from analysis.plotMap.mapData_yld import mapdateDict, mapdata


def geo_lines_background(dateMapDetal) -> Geo:
    c = (
        Geo()
            .add_schema(
            maptype="china",
            itemstyle_opts=opts.ItemStyleOpts(color="#323c48", border_color="#111"),
        )
            .add(
            "",
            # [("广州", 55), ("北京", 66), ("杭州", 77), ("重庆", 88)],
            dateMapDetal.get("dateNumList"),
            type_=ChartType.EFFECT_SCATTER,
            color="white",
        )
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True))
            .add(
            "geo",
            # [("广州", "上海"), ("广州", "北京"), ("广州", "杭州"), ("广州", "重庆")],
            dateMapDetal.get("dateFormToList"),
            type_=ChartType.LINES,
            effect_opts=opts.EffectOpts(
                symbol=SymbolType.ARROW, symbol_size=6, color="blue"
            ),
            linestyle_opts=opts.LineStyleOpts(curve=0.2),
        )
            .set_series_opts(label_opts=opts.LabelOpts(is_show=False))
            .set_global_opts(title_opts=opts.TitleOpts(title="召必回 战必胜 各地支援湖北医疗队实况",
                                                       subtitle="统计时间 2020/1/24-至今"),
                             graphic_opts=[
                                 opts.GraphicImage(
                                     graphic_item=opts.GraphicItem(
                                         id_="logo",
                                         right=20,
                                         top=20,
                                         z=-10,
                                         bounding="raw",
                                         origin=[75, 75],
                                     ),
                                     graphic_imagestyle_opts=opts.GraphicImageStyleOpts(
                                         image="https://twitter.com/NavigateNetwork/photo",
                                         width=150,
                                         height=150,
                                         opacity=0.4,
                                     ),
                                 )
                             ]
                             )
    )
    return c


from pyecharts.components import Table
from pyecharts.options import ComponentTitleOpts


def table_base(mapdata) -> Table:
    table = Table()
    headers = ["日期", "出发地", "目的地-省", "目的地-市", "医疗队人数"]
    # rows = [
    #     #     ["Brisbane", 5905, 1857594, 1146.4],
    #     #     ["Adelaide", 1295, 1158259, 600.5],
    #     #     ["Darwin", 112, 120900, 1714.7],
    #     #     ["Hobart", 1357, 205556, 619.5],
    #     #     ["Sydney", 2058, 4336374, 1214.8],
    #     #     ["Melbourne", 1566, 3806092, 646.9],
    #     #     ["Perth", 5386, 1554769, 869.4],
    #     # ]
    rows = []
    for item in mapdata.get("detail"):
        _ = [item.get('date'), item['from'], item['to_pro'], item['to_city'], item['num']]
        rows.append(_)

    table.add(headers, rows).set_global_opts(
        title_opts=ComponentTitleOpts(title="各地支援数据详情")
    )
    return table


from pyecharts.charts import Bar


def bar_base(mapdata) -> Bar:
    x, y = [], []
    for item in mapdata.get("detail"):
        x.append(item.get("from"))
        y.append(item.get("num"))

    c = (
        Bar()
            .add_xaxis(x)
            .add_yaxis("支援人数", y)
            .set_global_opts(title_opts=opts.TitleOpts(title="Bar-基本示例", subtitle="我是副标题"))
    )
    return c


tl = Timeline()
for k, v in mapdateDict.items():
    map0 = geo_lines_background(v)
    tl.add(map0, "{}".format(k))
#
# for item in mapdata:
#     table0 = bar_base(item)
#     tl.add(table0, "{}".format(item.get('date')))

tl.render('timeline_geo_yld.html')
