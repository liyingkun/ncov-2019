# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/14 5:42 下午
# @File     :mapData_yld.py
# @Software :PyCharm
""" 处理医疗队数据 """

import pandas as pd

df = pd.read_csv(r'/Users/yingkunli/Desktop/ncov-2019/analysis/output/yld_0214.csv')

df.columns

# 按时间划分
{"date": {"form": "", "to": "", "num": ""}}
{"date": {"form": "num"}}

from dfply import X, n, group_by, summarize, summarize_each, full_join, make_symbolic, left_join, n_distinct, first

dff = df >> group_by('date') >> summarize(detail=X.to_dict(orient='records'))

mapdata = dff.to_dict(orient='records')
mapdata[1]

mapdateDict = {}
for item in mapdata:
    date = item.get('date')
    dateNumList = []
    dateFormToList = []
    hbSumNum = 0
    for item2 in item.get('detail'):
        #
        _ = (item2.get('from'), item2.get('num'))
        hbSumNum += item2.get('num')
        dateNumList.append(_)
        #
        t_ = (item2.get('from'), item2.get('to_pro'))
        dateFormToList.append(t_)
    dateNumList.append(("湖北", hbSumNum))
    dateFormToList.append(("湖北", "湖北"))
    mapdateDict[date] = {"dateNumList": dateNumList, "dateFormToList": dateFormToList}
