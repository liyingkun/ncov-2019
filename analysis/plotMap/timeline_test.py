# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/14 4:57 下午
# @File     :timeline_test.py
# @Software :PyCharm
from pyecharts.charts import Timeline, Map, Geo
from pyecharts.faker import Faker
from pyecharts.globals import ChartType, SymbolType
from pyecharts import options as opts


def timeline_map() -> Timeline:
    tl = Timeline()
    for i in range(2015, 2020):
        map0 = (
            Map()
                .add(
                "商家A", [list(z) for z in zip(Faker.provinces, Faker.values())], "china"
            )
                .set_global_opts(
                title_opts=opts.TitleOpts(title="Map-{}年某些数据".format(i)),
                visualmap_opts=opts.VisualMapOpts(max_=200),
            )
        )
        tl.add(map0, "{}年".format(i))
    return tl


tl = timeline_map()
dir(tl)
tl.render()


def geo_lines_background() -> Geo:
    c = (
        Geo()
            .add_schema(
            maptype="china",
            itemstyle_opts=opts.ItemStyleOpts(color="#323c48", border_color="#111"),
        )
            .add(
            "",
            [("广州", 55), ("北京", 66), ("杭州", 77), ("重庆", 88)],
            type_=ChartType.EFFECT_SCATTER,
            color="white",
        )
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True))
            .add(
            "geo",
            [("广州", "上海"), ("广州", "北京"), ("广州", "杭州"), ("广州", "重庆")],
            type_=ChartType.LINES,
            effect_opts=opts.EffectOpts(
                symbol=SymbolType.ARROW, symbol_size=6, color="blue"
            ),
            linestyle_opts=opts.LineStyleOpts(curve=0.2),
        )
            .set_series_opts(label_opts=opts.LabelOpts(is_show=False))
            .set_global_opts(title_opts=opts.TitleOpts(title="Geo-Lines-background"))
    )
    return c


ge = geo_lines_background()

ge.render()

tl = Timeline()
for i in range(2015, 2020):
    map0 = geo_lines_background()
    tl.add(map0, "{}年".format(i))
tl.render('timeline_geo_test.html')
