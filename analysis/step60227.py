# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/27 5:43 下午
# @File     :step60227.py
# @Software :PyCharm

""" 有时间线 按天聚合 按周聚合 按发布的人聚合"""
import time
import urllib.parse
from analysis.db import mgdb
import pandas as pd
import numpy as np
from dfply import X, n, group_by, summarize, summarize_each, full_join, make_symbolic, left_join, n_distinct, first
from ncov.spiders.keywords import keywords_dict

sel = {'_id': 0, 'thumbnail': 0, 'content': 0}
df = pd.DataFrame(list(mgdb['posts'].find({}, sel)))

df['keywords'] = df.search_url.apply(lambda x: urllib.parse.unquote(x.split('?q=')[1].split('&')[0]))


def f_keywords(x):
    _ = x.split('|')
    if len(_) > 1:
        return _[0]
    else:
        return x


df['keywords'] = df.keywords.apply(f_keywords)


def f_int(x):
    try:
        if '万' in str(x):
            _ = str(x).replace('万+', '')
            return int(_) * 10000
        else:
            return int(x)
    except:
        print(x)
        return None


df['attitudes'] = df['attitudes'].apply(f_int)
df['comment'] = df['comment'].apply(f_int)
df['repost'] = df['repost'].apply(f_int)

# df = df.astype({'attitudes': 'int32', 'comment': 'int32', 'repost': 'int32'})

# 去重
df.drop_duplicates(subset=['keywords', 'url'], inplace=True)

# 行业补全，发现数据中行业数据为None
base_url = 'https://s.weibo.com/weibo?q={query}&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-13-0:2020-02-25-0'

start_urls_ls = []
for k, v in keywords_dict.items():
    for word in v:
        start_urls_dict = {}
        # url = base_url.format(query=urllib.parse.quote(word))
        start_urls_dict["keywords"] = word
        start_urls_dict["industryType"] = k
        start_urls_ls.append(start_urls_dict)

industrydf = pd.DataFrame(start_urls_ls)

df = df >> left_join(industrydf, by=['keywords'])

df['postMediavalue'] = df['repost'] + df['comment'] + df['attitudes']

# 判断是否和疫情有关

import re


def f_relation(x):
    """ 判断是否和舆情相关 """
    return re.findall('疫情|武汉|捐|赠|肺炎|口罩|钟南山|李兰娟|病毒|支援|加油|湖北|远程办公', x)


def f_is_relation(x):
    if len(x) >= 1:
        return '相关'
    else:
        return '不相关'


df['ncovkeywords'] = df['title'].apply(f_relation)
df['isncovkeywords'] = df['ncovkeywords'].apply(f_is_relation)

# df
df.columns
df['created_at'].value_counts()


def f_sj(x):
    """处理时间格式"""
    if re.match('今天\d{2}:\d{2}', x):
        date = time.strftime('%Y-%m-%d ', time.localtime()) + x[2:]
        return date[0:10]
    else:
        return x[0:10]


df['day'] = pd.to_datetime(df['created_at'].apply(f_sj))


# 按日聚合

# 区分热门贴还是媒体贴

def f_cat(x):
    if 'hot' in x:
        return '热门贴'
    elif 'category' in x:
        return '媒体贴'
    else:
        return '其它'


df['postCat'] = df['search_url'].apply(f_cat)

# 分类聚合汇总
stadf = df >> group_by('industryType', 'keywords', 'isncovkeywords', 'postCat', 'day') >> \
        summarize_each([np.sum], 'repost', 'comment', 'attitudes', 'postMediavalue')

sumdf = df >> group_by('industryType', 'keywords', 'isncovkeywords', 'postCat', 'day') >> \
        summarize(post_num=n(X.url)) >> full_join(stadf,
                                                  by=['industryType', 'keywords', 'isncovkeywords', 'postCat', 'day'])


def roud(x):
    # print(type(x))
    if type(x) == float:
        return round(x, 0)
    else:
        return x


# _a = sumdf.applymap(lambda x: roud(x)) # 报错

# 新增一个封城前后
def f_fc(x):
    if x <= pd.to_datetime('2020-1-22'):
        return '封城前'
    else:
        return '封城后'


sumdf['timeFlag'] = sumdf['day'].apply(f_fc)


####################################
# 提取出文章发布者
def f_user(x):
    return x.split('https://weibo.com/')[1].split('/')[0]


# 1742566624，316593888，1687422352，2892425653
df['userid'] = df['url'].apply(f_user)

df['userid'].value_counts()[20:100]

# 按行业，提取出发文量前3的作者
ddf = df >> group_by('industryType', 'userid') >> \
      summarize(post_num=n(X.url))
ddf.sort_values(by=['post_num'], ascending=False).head(100)['industryType'].value_counts()
userlist = ddf.sort_values(by=['post_num'], ascending=False).head(1000)['userid'].values

ddf2 = df[df['userid'].isin(userlist)]
# 活跃度：发文量的综合值，映射到85-95的区间
# 1.行业活跃度，用sumdf来弄，发文量和媒体价值，来映射，映射后求平均
# 2.行业中某个网红的活跃度，发文量和媒体价值，来映射，映射后求平均
staddf = ddf2 >> group_by('industryType', 'userid', 'day') >> \
         summarize_each([np.sum], 'repost', 'comment', 'attitudes', 'postMediavalue')

sumddf = ddf2 >> group_by('industryType', 'userid', 'day') >> \
         summarize(post_num=n(X.url)) >> full_join(staddf,
                                                   by=['industryType', 'userid', 'day'])
'''
ymaxymax  要映射的目标区间最大值
yminymin 要映射的目标区间最小值
xmaxxmax 目前数据最大值
xminxmin 目前数据最小值
xx 假设目前数据中的任一值
yy 归一化映射后的值

y=ymin+ymax−yminxmax−xmin∗(x−xmin)

'''


def f_post_activity(x, xmax=sumddf['post_num'].mean(), xmin=sumddf['post_num'].min(), ymax=100, ymin=80):
    _ = ymin + ((ymax - ymin) * (x - xmin)) / (xmax - xmin)
    if _ > ymax:
        return ymax
    else:
        return _


def f_media_activity(x, xmax=sumddf['postMediavalue_sum'].mean(), xmin=sumddf['postMediavalue_sum'].min(), ymax=100,
                     ymin=80):
    _ = ymin + ((ymax - ymin) * (x - xmin)) / (xmax - xmin)
    if _ > ymax:
        return ymax
    else:
        return _


sumddf['post_activity'] = sumddf['post_num'].apply(f_post_activity)
sumddf['Mediavalue_activity'] = sumddf['postMediavalue_sum'].apply(f_media_activity)
sumddf['activity'] = (sumddf['post_activity'] + sumddf['Mediavalue_activity']) / 2

sumddf['post_activity'].describe()
sumddf['activity'].describe()

sumddf['timeFlag'] = sumdf['day'].apply(f_fc)

# 保留前后都有的
xx = sumddf >> group_by('industryType', 'userid') >> \
     summarize(have_num=n_distinct(X.timeFlag))

xx.columns
uid = list(set(xx.query('have_num==2')['userid'].values))
sumddf2 = sumddf[sumddf['userid'].isin(uid)]

xx2 = sumddf2 >> group_by('industryType') >> \
      summarize(user_id=first(X.userid))
uid2 = list(set(xx2['user_id'].values))
sumddf3 = sumddf2[sumddf2['userid'].isin(uid2)]

xx.query('userid=="1314608344"')
"""
# 使用标准正态分布来映射
from scipy.stats import norm


def f_post_activity(x, xmean=sumddf['post_num'].mean(), xstd=sumddf['post_num'].std()):
    return norm.cdf((x - xmean) / xstd)
sumddf['post_num'].apply(f_post_activity).describe()
"""

# 活跃度计算精确到品牌

staddf2 = df.query('industryType=="美容美妆"') >> group_by('industryType', 'userid', 'day', 'keywords') >> \
          summarize_each([np.sum], 'repost', 'comment', 'attitudes', 'postMediavalue')

sumddf2 = df.query('industryType=="美容美妆"') >> group_by('industryType', 'userid', 'day', 'keywords') >> \
          summarize(post_num=n(X.url)) >> full_join(staddf2,
                                                    by=['industryType', 'userid', 'day', 'keywords'])


def f_post_activity(x, xmax=sumddf2['post_num'].mean(), xmin=sumddf2['post_num'].min(), ymax=100, ymin=80):
    _ = ymin + ((ymax - ymin) * (x - xmin)) / (xmax - xmin)
    if _ > ymax:
        return ymax
    else:
        return _


def f_media_activity(x, xmax=sumddf2['postMediavalue_sum'].mean(), xmin=sumddf2['postMediavalue_sum'].min(), ymax=100,
                     ymin=80):
    _ = ymin + ((ymax - ymin) * (x - xmin)) / (xmax - xmin)
    if _ > ymax:
        return ymax
    else:
        return _


sumddf2['post_activity'] = sumddf2['post_num'].apply(f_post_activity)
sumddf2['Mediavalue_activity'] = sumddf2['postMediavalue_sum'].apply(f_media_activity)
sumddf2['activity'] = (sumddf2['post_activity'] + sumddf2['Mediavalue_activity']) / 2

sumddf2['post_activity'].describe()
sumddf2['activity'].describe()

if __name__ == "__main__":
    # _a.to_csv('/Users/yingkunli/Desktop/ncov-2019/analysis/output/step20206.csv', index=False, encoding='gbk')
    # sumdf.to_excel('/Users/yingkunli/Desktop/ncov-2019/analysis/output/舆情报告-0213.xlsx', index=False, encoding='gbk')
    # sumddf.to_excel('/Users/yingkunli/Desktop/ncov-2019/analysis/output/activity-0213.xlsx', index=False,
    #                 encoding='gbk')
    with pd.ExcelWriter('/Users/yingkunli/Desktop/ncov-2019/analysis/output/数据报告-raw-0227.xlsx') as writer:
        # sumdf.to_excel(writer, sheet_name='原始聚合数据')
        # sumddf.to_excel(writer, sheet_name='原始活跃度数据')
        # sumddf3.to_excel(writer, sheet_name='行业代表活跃度数据')
        sumddf2.to_excel(writer, sheet_name='化妆品品牌活跃度')
