# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/8 10:17 上午
# @File     :yl.py
# @Software :PyCharm

""" 医疗数据分析 """
from analysis.db import mgclient
import pandas as pd

df = pd.DataFrame(list(mgclient['yiliao']['yl'].find({}, {'_id': 0})))
df.to_excel('/Users/yingkunli/Desktop/ncov-2019/analysis/output/医疗数据.xlsx', index=False)
