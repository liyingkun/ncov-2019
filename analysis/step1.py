# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/4 5:08 下午
# @File     :step1.py
# @Software :PyCharm
import urllib.parse

from analysis.db import mgdb
import pandas as pd
import numpy as np
from dfply import X, n, group_by, summarize, summarize_each, full_join, make_symbolic

sel = {'_id': 0, 'thumbnail': 0, 'content': 0}
df = pd.DataFrame(list(mgdb['posts'].find({}, sel)))
df.columns
df.info()
df.dtypes
df.repost[0]

df['keywords'] = df.search_url.apply(lambda x: urllib.parse.unquote(x.split('?q=')[1].split('&')[0]))


def f_keywords(x):
    _ = x.split('+')
    if len(_) > 1:
        return _[0]
    else:
        return x


df['keywords'] = df.keywords.apply(f_keywords)

df = df.astype({'attitudes': 'int32', 'comment': 'int32', 'repost': 'int32'})

# 去重
df.drop_duplicates(subset=['keywords', 'url'], inplace=True)

df['postMediavalue'] = df['repost'] + df['comment'] + df['attitudes']

stadf = df >> group_by('keywords') >> \
        summarize_each([np.sum, np.mean, np.max], 'repost', 'comment', 'attitudes', 'postMediavalue')

sumdf = df >> group_by('keywords') >> \
        summarize(post_num=n(X.url)) >> full_join(stadf, by=['keywords'])


def roud(x):
    # print(type(x))
    if type(x) == float:
        return round(x, 0)
    else:
        return x


_a = sumdf.applymap(lambda x: roud(x))

df.info()
_a.info()
_a.keywords

_a.to_csv('/Users/yingkunli/Desktop/ncov-2019/analysis/output/step1.csv', index=False, encoding='gbk')
