# --*-- coding: utf-8 _*_
# @Author   :liyingkun
# @time     :2020/2/7 8:50 上午
# @File     :step30207.py
# @Software :PyCharm

""" 今日将新增热门贴和媒体贴 """

import urllib.parse
from analysis.db import mgdb
import pandas as pd
import numpy as np
from dfply import X, n, group_by, summarize, summarize_each, full_join, make_symbolic, left_join
from ncov.spiders.keywords import keywords_dict

sel = {'_id': 0, 'thumbnail': 0, 'content': 0}
df = pd.DataFrame(list(mgdb['posts'].find({}, sel)))

df['keywords'] = df.search_url.apply(lambda x: urllib.parse.unquote(x.split('?q=')[1].split('&')[0]))


def f_keywords(x):
    _ = x.split('|')
    if len(_) > 1:
        return _[0]
    else:
        return x


df['keywords'] = df.keywords.apply(f_keywords)


def f_int(x):
    try:
        if '万' in str(x):
            _ = str(x).replace('万+', '')
            return int(_) * 10000
        else:
            return int(x)
    except:
        print(x)
        return None


df['attitudes'] = df['attitudes'].apply(f_int)
df['comment'] = df['comment'].apply(f_int)
df['repost'] = df['repost'].apply(f_int)

# df = df.astype({'attitudes': 'int32', 'comment': 'int32', 'repost': 'int32'})

# 去重
df.drop_duplicates(subset=['keywords', 'url'], inplace=True)

# 行业补全，发现数据中行业数据为None
base_url = 'https://s.weibo.com/weibo?q={query}&Refer=SWeibo_box&typeall=1&suball=1&timescope=custom:2020-01-01-0:2020-02-05-0'

start_urls_ls = []
for k, v in keywords_dict.items():
    for word in v:
        start_urls_dict = {}
        # url = base_url.format(query=urllib.parse.quote(word))
        start_urls_dict["keywords"] = word
        start_urls_dict["industryType"] = k
        start_urls_ls.append(start_urls_dict)

industrydf = pd.DataFrame(start_urls_ls)

df = df >> left_join(industrydf, by=['keywords'])

df['postMediavalue'] = df['repost'] + df['comment'] + df['attitudes']

# 判断是否和疫情有关

import re


def f_relation(x):
    """ 判断是否和舆情相关 """
    return re.findall('疫情|武汉|捐|赠|肺炎|口罩|钟南山|李兰娟|病毒|支援|加油|湖北|远程办公', x)


def f_is_relation(x):
    if len(x) >= 1:
        return '相关'
    else:
        return '不相关'


df['ncovkeywords'] = df['title'].apply(f_relation)
df['isncovkeywords'] = df['ncovkeywords'].apply(f_is_relation)


# 区分热门贴还是媒体贴

def f_cat(x):
    if 'hot' in x:
        return '热门贴'
    elif 'category' in x:
        return '媒体贴'
    else:
        return '其它'


df['postCat'] = df['search_url'].apply(f_cat)

# 分类聚合汇总
stadf = df >> group_by('industryType', 'keywords', 'isncovkeywords', 'postCat') >> \
        summarize_each([np.sum], 'repost', 'comment', 'attitudes', 'postMediavalue')

sumdf = df >> group_by('industryType', 'keywords', 'isncovkeywords', 'postCat') >> \
        summarize(post_num=n(X.url)) >> full_join(stadf, by=['industryType', 'keywords', 'isncovkeywords', 'postCat'])


def roud(x):
    # print(type(x))
    if type(x) == float:
        return round(x, 0)
    else:
        return x


_a = sumdf.applymap(lambda x: roud(x))
_a.post_num.sum()
df.info()
df.keywords.value_counts()
_a.columns

## 关键词，词频统计
"""
import jieba

import jieba.analyse


def topNwords(x, n=20):
    data = re.sub(r"[\s+\.\!\/_,$%^*(【】：\]\[\-:;+\"\']+|[+——！，。？、~@#￥%……&*（）]+|[0-9]+|收起全文", "", x)
    return jieba.analyse.extract_tags(data, topK=n, allowPOS=('ns', 'n', 'nr', 'nt', 'nz'), withWeight=True)


@make_symbolic
def add_content(x):
    # print(x)
    # print(type(x))
    return '    '.join(x.values)


# df['title'].apply(topNwords)
dd = df >> group_by('industryType', 'keywords', 'isncovkeywords') >> \
     summarize_each([add_content], 'title')

dd['content_tags'] = dd.title_wrapper.apply(topNwords).apply(str)
selcol = ['isncovkeywords', 'keywords', 'industryType',
          'content_tags']
df.info()
"""
if __name__ == "__main__":
    # _a.to_csv('/Users/yingkunli/Desktop/ncov-2019/analysis/output/step20206.csv', index=False, encoding='gbk')
    _a.to_excel('/Users/yingkunli/Desktop/ncov-2019/analysis/output/舆情报告-0207.xlsx', index=False, encoding='gbk')
